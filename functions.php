<?php
/**
 * Bard child theme custom functions
 */
//add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
//function my_theme_enqueue_styles() {
//	$parent_style = 'parent-style';
//	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', 10 );
//	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/build/style.css', 100  );
//}

// enqueue styles for child theme
//function bardchild_enqueue_styles() {
//
//	// enqueue parent styles
//	wp_enqueue_style('parent-theme', get_template_directory_uri() .'/style.css');
//
//	// enqueue child styles
//	//wp_enqueue_style('child-theme', get_stylesheet_directory_uri() .'/build/style.css', array('parent-theme'));
//	wp_register_style('child-theme', get_stylesheet_directory_uri() .'/build/style.css', array('parent-theme'));
//	wp_enqueue_style('child-theme');
//
//}
//add_action('wp_enqueue_scripts', 'bardchild_enqueue_styles');

// Queue parent style followed by child/customized style
//parent style first
function parent_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'parent_theme_enqueue_styles', 10);

// then load child style with maximum priority over parent
function child_theme_enqueue_styles() {
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/build/style.css', array( 'parent-style' ) );
	wp_enqueue_script('child-style', get_stylesheet_directory_uri() . '/build/main.js', array( ), null, true );
}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles', PHP_INT_MAX);



//function my_theme_scripts()
//{
//	wp_enqueue_script('bardchild', get_stylesheet_directory_uri() . '/build/main.js', array( ), null, true );
//}
//add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );



function my_theme_contionnal_scripts()
{
	if (is_page('pagenamehere')) {
		wp_register_script('bardchild_script', get_template_directory_uri() . '/build/main.js', array('jquery'), '1.0.0','true'); // Conditional script(s)
		wp_enqueue_script('bardchild_script'); // Enqueue it!
	}
}


//wp_enqueue_script('bardchild', get_stylesheet_directory_uri() . '/build/main.js', array( ), null, true );

//Remove admin bar on front-end
show_admin_bar( false);

// Favicon function to call in the header
function favicon_link() {
	echo '<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />' . "\n";
}
add_action( 'wp_head', 'favicon_link' );


?>
<?php
/**
 * Pluggable functions to overwrite functions already defined in parent theme
 *
 */
if ( !is_defined( 'my_meta' ) ) {
	function my_meta() {
		// code for postmeta here
	}
}
?>